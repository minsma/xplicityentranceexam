using System;
using System.Collections.Generic;

namespace Xplicity.Academy.EntranceExam.Tasks
{
    public class Task2 : ITask2
    {
        public bool Execute(string value)
        {
            int maxElementsInSubstring = value.Length / 2;

            for (int i = 1; i <= maxElementsInSubstring; i++)
            {
                string word = value;
                List<string> substrings = new List<string>();

                while (word.Length > 0)
                {
                    substrings.Add(word.Substring(0, i));
                    word = word.Remove(0, i);
                }

                string theFirstSequenceMember = substrings[0];
                int count = 0;

                for (int j = 1; j < substrings.Count; j++)
                {
                    if (theFirstSequenceMember.Equals(substrings[j]))
                    {
                        count++;
                    }
                }

                if (count == substrings.Count - 1)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
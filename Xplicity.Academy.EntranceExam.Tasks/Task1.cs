using System;

namespace Xplicity.Academy.EntranceExam.Tasks
{
    public class Task1 : ITask1
    {
        public double CalculateArea(double a, double b, double c)
        {
            double area = -1;

            if (a > 0 && b > 0 && c > 0 && c > b)
            {
                area = 1.0 / 2.0 * b * Math.Sqrt(Math.Pow(c, 2) - Math.Pow(b, 2));
            }

            return Math.Round(area, 2);
        }
    }
}
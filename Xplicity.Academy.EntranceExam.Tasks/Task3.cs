using System;
using System.Collections.Generic;

namespace Xplicity.Academy.EntranceExam.Tasks
{
    public class Task3 : ITask3
    {
        public int GetHashCode(IEnumerable<object> source)
        {
            int hashCode = 0;

            if (source != null)
            {
                using (IEnumerator<object> enumerator = source.GetEnumerator())
                {
                    while (enumerator.MoveNext())
                    {
                        if (enumerator.Current != null)
                        {
                            hashCode += enumerator.Current.GetHashCode();
                        }
                    }
                }
            }

            return hashCode;
        }
    }
}
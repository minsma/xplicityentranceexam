using Xplicity.Academy.EntranceExam.Tasks;
using Xunit;

namespace Xplicity.Academy.EntranceExam.Tests
{
    [Trait("Entrance Exam", "Tasks")]
    public class TaskTests
    {
        [Theory]
        [InlineData(6, 3, 4, 5)]
        public void ItReturns_ExpectedArea_BySizes(double area, double a, double b, double c)
        {
            //Prepare:

            var task1 = new Task1();

            //Act:

            var actual = task1.CalculateArea(a, b, c);

            //Assert:

            Assert.Equal(area, actual);
        }

        [Theory]
        [InlineData(true, "abcabc")]
        [InlineData(true, "a1a1a1")]
        [InlineData(true, "qq")]
        [InlineData(false, "z")]
        public void ItReturns_ExpectedResult_ByValue(bool result, string value)
        {
            //Prepare:

            var task2 = new Task2();

            //Act:

            var actual = task2.Execute(value);

            //Assert:

            Assert.Equal(result, actual);
        }

        [Theory]
        [InlineData(new object[] {1, 2}, new object[] {2, 1})]
        [InlineData(new object[] {"qwerty", 5, true}, new object[] {5, true, "qwerty"})]
        [InlineData(new object[] {null, 5, null}, new object[] {null, null, 5})]
        [InlineData(new object[] {null}, new object[] {null})]
        public void ItReturns_SameResult_ForIdenticalArrays(object[] array1, object[] array2)
        {
            //Prepare:

            var task3 = new Task3();

            //Act:

            var hashCode1 = task3.GetHashCode(array1);

            var hashCode2 = task3.GetHashCode(array2);

            //Assert:

            Assert.True(hashCode1 == hashCode2);
        }
    }
}
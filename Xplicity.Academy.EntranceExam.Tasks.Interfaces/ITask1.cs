namespace Xplicity.Academy.EntranceExam.Tasks
{
    /// <summary>
    /// Do not change this interface
    /// </summary>
    public interface ITask1
    {
        double CalculateArea(double a, double b, double c);
    }
}
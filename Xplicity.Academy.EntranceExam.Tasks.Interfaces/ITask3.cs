using System.Collections.Generic;

namespace Xplicity.Academy.EntranceExam.Tasks
{
    /// <summary>
    /// Do not change this interface
    /// </summary>
    public interface ITask3
    {
        int GetHashCode(IEnumerable<object> source);
    }
}
namespace Xplicity.Academy.EntranceExam.Tasks
{
    /// <summary>
    /// Do not change this interface
    /// </summary>
    public interface ITask2
    {
        bool Execute(string value);
    }
}
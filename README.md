# Xplicity Entrance Exam

# 1 Task

Implement a function, which can calculate an area of the triangle by the specified lengths of
three lines. Round the result until the second digit after dot (for example, "5.12"). In case of
invalid input data, return -1.

# 2 Task

By the specified Unicode string decide, if this string is the periodic sequence with at least two
elements, i.e. does it fully consist of identical substrings. Examples: "abcabc", "a1a1a1", "qq",
“5555”. Return true, if the string is sequence, otherwise false. In case of wrong input data,
return false.

# 3 Task

Write a function that returns a hash calculated out of specified set of objects
(IEnumerable<object>). The input data cannot be null or empty. The hash function must return
values from [-15 ... 16] range only. The hash function must return the same value for identical
sets of objects. In case of non-identical sets, the hash function should return data from the
entire range. If the amount of collisions is bigger than statistically expected amount, it is a
disadvantage of the hash-function.
Two arrays are identical, if they contain the same elements (no matter in which order). Some
elements can be null. Consider null elements the same elements. Example of identical arrays:
{1, 2} and {2, 1}, {"qwerty", 5, true} and {5, true, "qwerty"}, {null, 5, null} and {null, null, 5}, {null}
and {null}